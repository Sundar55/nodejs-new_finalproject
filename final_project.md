<img src="../images/IDSNlogo.png" width="200" height="200"/>


# Final Project

## Estimated Effort: 1 hr. 30 mins.

## Part A: Fork the Git repository to have the server code you need to start

1. Go to the [repository](mxpfu-nodejsLabs/final_project/) which has the partially developed code for server side. 

2. Create a fork of the repository into your own.

3. Copy the **url** of your repository. **This needs to be submitted as part of the final submission.**

<img src="images/" style="margin-left:30px"/>

4. Go to your repository and copy the clone url. 

<img src="images/" style="margin-left:30px"/>

5. Open a terminal.

<img src="images/" width="75%" style="margin-left:30px"/>

6. Clone the repository by pasting the url you copied from the git repository.

    ```
    git clone <your_repo_name>
    ```

<img src="images/" width="75%" style="margin-left:30px"/>

7. This will clone the repository with server and client files in your home directory in the lab environment. Check the same with the following command.

    ```
    ls
    ```
{: codeblock}

The output should list the directory named `final-project`. 

8. Run the following command to update npm version.
    ```
    npm install npm@latest -s
    ```
{: codeblock}

9. Change to the project directory and check its contents.

    ```
    cd final_project && ls
    ```
{: codeblock}

<img src="images/" width="75%" style="margin-left:30px"/>



# Part B: Install Server-side packages

1. Change to the server directory.
    ```
    cd /home/project/mxpfu-nodejsLabs/final_project/sentimentAnalyzeServer/
    ```
{: codeblock}

2. All packages are needed to be installed are listed in `package.json`. Run `npm install -s`, to install and save those packages.

    ```
    npm install  -s
    ```
{: codeblock}


3. Run the server using the following command.
    ```
    npm start
    ```
{: codeblock}

4. If all the required packages are successfully installed, the server should start without any issues.

<img src="images/" width="75%" style="margin-left:30px"/>

5. Press Ctrl+C to stop the server.


# Part C: Install IBM Watson package and set up the .env file

1. Install the ibm-watson package in your server side using the following command.

    ```
    npm install  -s ibm-watson@6.1.1
    ```
{: codeblock}

<img src="images/" width="75%" style="margin-left:30px"/>


2. Right click on the `sentimentAnalyzeServer` in the explorer and create a new file named `.env`.

<img src="images/" width="75%" style="margin-left:30px"/>

3. Copy the credentials to point to your Watson NLU credentials from the IBM cloud, if you have not already done so in the previous lab.

<img src="images/screenshot-NLUcreds.png" width="75%" style="margin-left:30px"/>

4. Add the credentials required to the .env file you created.

<img src="images/screenshot-envfile.png" width="75%" style="margin-left:30px"/>

5. The contents of the .env file would be as below
    ```txt
    API_KEY=<your api key>
    API_URL=<your url>
    ```

6. Install dotenv to use the .env file in the server application using the following command.

    ```
    npm install  -s dotenv@10.0.0
    ```
{: codeblock}

<img src="images/screenshot-npminstalldotenv.png" width="75%" style="margin-left:30px"/>


7. Make changes in your express JS (in the file named sentimentAnalyzerServer.js) to define and implement a method `getNLUInstance()` where you create an instance of NaturalLanguageUnderstanding using the credential from the .env file using dotenv package and return the instance. You can refer to this [link](https://cloud.ibm.com/apidocs/natural-language-understanding?code=node) for more details on how to do this.

```js
    const NaturalLanguageUnderstandingV1 = require('ibm-watson/natural-language-understanding/v1');
    const { IamAuthenticator } = require('ibm-watson/auth');

    const naturalLanguageUnderstanding = new NaturalLanguageUnderstandingV1({
        version: '2021-08-01',
        authenticator: new IamAuthenticator ({
            apikey: api_key
        }),
        serviceUrl: api_url
    });
    return naturalLanguageUnderstanding;
```
{: codeblock}

# Part D: Create end points

1. Observe that the server code intends to provide 4 end-points. One of the end-points has been provided for you. Uncomment it and observe the code. It is highly recommended that you attempt to implement these end-points by yourself in **sentimentAnalyzerServer.js** before you use the code given below to complete the assignment. 

>These are points you should keep in mind, while implementing the end-points.
* Each of the end points should use the `getNLUInstance()` created in the previous part to return appropriate response.
* Each end point should send a JSON or a string response as may be considered relevant.
* The JSON object returned from the IBM NaturalLanguageUnderstanding API is as in the below image. We need to extract the information that we need.
    <img src="images/url_emotion_json.png"/>


2. `/url/emotion` end-point is used to analyze the **emotion** using NLU, for a given url. 

```js
    app.get("/url/emotion", (req,res) => {
        let urlToAnalyze = req.query.url
        const analyzeParams = 
        {
            "url": urlToAnalyze,
            "features": {
                "keywords": {
                    "emotion": true,
                    "limit": 1
                }
            }
        }
        
        const naturalLanguageUnderstanding = getNLUInstance();
        
        naturalLanguageUnderstanding.analyze(analyzeParams)
        .then(analysisResults => {
            //Retrieve the emotion and return it as a formatted string
            return res.send(analysisResults.result.keywords[0].emotion,null,2);
        })
        .catch(err => {
            return res.send("Could not do desired operation "+err);
        });
    });

```

{: codeblock}

3. `/url/sentiment` end-point is used to analyze the **sentiment** using NLU, for a given url.

```js
    app.get("/url/sentiment", (req,res) => {
        let urlToAnalyze = req.query.url
        const analyzeParams = 
        {
            "url": urlToAnalyze,
            "features": {
                "keywords": {
                    "sentiment": true,
                    "limit": 1
                }
            }
        }
        
        const naturalLanguageUnderstanding = getNLUInstance();
        
        naturalLanguageUnderstanding.analyze(analyzeParams)
        .then(analysisResults => {
            //Retrieve the sentiment and return it as a formatted string

            return res.send(analysisResults.result.keywords[0].sentiment,null,2);
        })
        .catch(err => {
            return res.send("Could not do desired operation "+err);
        });
    });
```

{: codeblock}

4. `/text/emotion` end-point is used to analyze the **emotion** using NLU, for a given text.

```js
    app.get("/text/emotion", (req,res) => {
        let textToAnalyze = req.query.text
        const analyzeParams = 
        {
            "text": textToAnalyze,
            "features": {
                "keywords": {
                    "emotion": true,
                    "limit": 1
                }
            }
        }
        
        const naturalLanguageUnderstanding = getNLUInstance();
        
        naturalLanguageUnderstanding.analyze(analyzeParams)
        .then(analysisResults => {
            //Retrieve the emotion and return it as a formatted string

            return res.send(analysisResults.result.keywords[0].emotion,null,2);
        })
        .catch(err => {
            return res.send("Could not do desired operation "+err);
        });
    });
```
{: codeblock}

5. `/text/sentiment` end-point is used to analyze the **sentiment** using NLU, for a given text.

```js
    app.get("/text/sentiment", (req,res) => {
        let textToAnalyze = req.query.text
        const analyzeParams = 
        {
            "text": textToAnalyze,
            "features": {
                "keywords": {
                    "sentiment": true,
                    "limit": 1
                }
            }
        }
        
        const naturalLanguageUnderstanding = getNLUInstance();
        
        naturalLanguageUnderstanding.analyze(analyzeParams)
        .then(analysisResults => {
            //Retrieve the sentiment and return it as a formatted string

            return res.send(analysisResults.result.keywords[0].sentiment,null,2);
        })
        .catch(err => {
            return res.send("Could not do desired operation "+err);
        });
    });
```
{: codeblock}






6. Run the server. This will start on port `8080`.
    ```
    npm start
    ```
{: codeblock}

7. Click on the **Skills Network** button on the left, it will open the "Skills Network Toolbox". Then click the **Other** then **Launch Application**. From there you should be able to enter the port `8080`. 

<img src="images/Launch_Application-SN-ToolBox.png"  width="75%" style="margin-left:30px"/>

Verify your output on the browser. The output depending on which whether you choose text or URL and whether you choose sentiment or emotions, the output should vary.

<img src="images/screenshot-reactclientthroughserver.png"  width="75%" style="margin-left:30px"/>

8. Please refer to this lab [ Working with git in the Theia lab environment](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CD0101EN-SkillsNetwork/labs/GitHubLabs/Github_commit.md.html)



7. Share the URL of your Git repository with all the changes. *Ensure that the .env file with all the credentials is not added to the gitrepository



# Checklist for completion

* Client-side changes done as per requirements.
    * Title changed to `Sentiment Analyzer`
    * Changed the color of the sentiments
    * Display response for emotion as a table

* Client side `npm run-script build` is run to build the client code and copy it to server side.
* .env file is created on the server side and the NLU credentials are added.
* All the four API endpoints have been implemented in the server side.
* Testing done from server side to see if the client is rendered as expected - Text Emotion, Text Sentiment, URL Emotions and URL Sentiment return results as expected.
* Code is pushed to Github.

# Next step
The next and the final step is the deployment of the application on cloud foundry. 


## Author(s)
Sapthashree K S,

K Sundararajan


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 2022-09-05 | 1.0 | K Sundararajan | Initial version created |


## <h3 align="center"> © IBM Corporation 2020. All rights reserved. <h3/>
